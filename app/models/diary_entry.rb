# frozen_string_literal: true

# DiaryEntries track day to day mood of the Loser, and
# how well they stick to their goals.
class DiaryEntry < ApplicationRecord
  belongs_to :diary

  validates :sleep, :hunger, :stress, :energy, numericality: {
    only_integer: true, greater_than_or_equal_to: 1, less_than_or_equal_to: 10
  }
end
