# frozen_string_literal: true

# Goal describes a weight goal for a Loser. Every Loser has
# one Goal that they can track progress against.
class Goal < ApplicationRecord
  belongs_to :loser
end
