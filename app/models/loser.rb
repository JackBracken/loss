# frozen_string_literal: true

# Someone working towards a weight-loss goal.
class Loser < ApplicationRecord
  has_one :diary, dependent: :destroy
  has_one :goal,  dependent: :destroy

  after_create :create_diary, :create_goal

  enum sex: { male: 0, female: 1, unspecified: 2 }
end
