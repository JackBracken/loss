# frozen_string_literal: true

# A WeighIn is a daily measurement of a Loser's weight.
class WeighIn < ApplicationRecord
  belongs_to :diary
end
