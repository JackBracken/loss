# frozen_string_literal: true

# Measurements are used to track progress over time. When
# checking in at the end of each week, a Loser should take
# measurements across their entire body. We can use these
# to track body recomposition progress.
class Measurement < ApplicationRecord
  belongs_to :diary

  enum type: {
    chest:       0,
    left_arm:    1,
    right_arm:   2,
    above_navel: 3,
    navel:       4,
    below_navel: 5,
    hips:        6,
    left_thigh:  7,
    right_thigh: 8
  }
end
