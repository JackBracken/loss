# frozen_string_literal: true

# The Diary is just a container for WeighIns, Measurements,
# and DiaryEntries. Every Loser has one Diary.
class Diary < ApplicationRecord
  belongs_to :loser

  has_many :weigh_ins,     dependent: :destroy
  has_many :measurements,  dependent: :destroy
  has_many :diary_entries, dependent: :destroy

  alias_attribute :entries, :diary_entries
end
