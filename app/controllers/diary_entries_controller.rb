# frozen_string_literal: true

# :nodoc
class DiaryEntriesController < ApplicationController
  before_action :set_diary_entry, only: %i[show update destroy]

  # GET /diary_entries
  def index
    @diary_entries = DiaryEntry.all

    render json: @diary_entries
  end

  # GET /diary_entries/1
  def show
    render json: @diary_entry
  end

  # POST /diary_entries
  def create
    @diary_entry = DiaryEntry.new(diary_entry_params)

    if @diary_entry.save
      render json: @diary_entry, status: :created, location: @diary_entry
    else
      render json: @diary_entry.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /diary_entries/1
  def update
    if @diary_entry.update(diary_entry_params)
      render json: @diary_entry
    else
      render json: @diary_entry.errors, status: :unprocessable_entity
    end
  end

  # DELETE /diary_entries/1
  def destroy
    @diary_entry.destroy
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_diary_entry
    @diary_entry = DiaryEntry.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def diary_entry_params
    params.require(:diary_entry).permit(:calories, :steps, :sleep, :hunger, :stress, :energy, :notes)
  end
end
