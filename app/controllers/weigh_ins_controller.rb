# frozen_string_literal: true

# :nodoc
class WeighInsController < ApplicationController
  before_action :set_weigh_in, only: %i[show update destroy]

  # GET /weigh_ins
  def index
    @weigh_ins = WeighIn.all

    render json: @weigh_ins
  end

  # GET /weigh_ins/1
  def show
    render json: @weigh_in
  end

  # POST /weigh_ins
  def create
    @weigh_in = WeighIn.new(weigh_in_params)

    if @weigh_in.save
      render json: @weigh_in, status: :created, location: @weigh_in
    else
      render json: @weigh_in.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /weigh_ins/1
  def update
    if @weigh_in.update(weigh_in_params)
      render json: @weigh_in
    else
      render json: @weigh_in.errors, status: :unprocessable_entity
    end
  end

  # DELETE /weigh_ins/1
  def destroy
    @weigh_in.destroy
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_weigh_in
    @weigh_in = WeighIn.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def weigh_in_params
    params.require(:weigh_in).permit(:weight)
  end
end
