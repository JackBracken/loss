# frozen_string_literal: true

# :nodoc
class GoalsController < ApplicationController
  before_action :set_goal, only: %i[show update destroy]

  # GET /goals
  def index
    @goals = Goal.all

    render json: @goals
  end

  # GET /goals/1
  def show
    render json: @goal
  end

  # POST /goals
  def create
    @goal = Goal.new(goal_params)

    if @goal.save
      render json: @goal, status: :created, location: @goal
    else
      render json: @goal.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /goals/1
  def update
    if @goal.update(goal_params)
      render json: @goal
    else
      render json: @goal.errors, status: :unprocessable_entity
    end
  end

  # DELETE /goals/1
  def destroy
    @goal.destroy
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_goal
    @goal = current_goal
  end

  # Only allow a trusted parameter "white list" through.
  def goal_params
    params.require(:goal).permit(:weight)
  end
end
