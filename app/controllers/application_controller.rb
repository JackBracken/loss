# frozen_string_literal: true

# :nodoc
class ApplicationController < ActionController::API
  # TODO: this is for convenience only, replace with devise
  # asap
  def current_loser
    Loser.find(params[:loser_id])
  end

  def current_goal
    current_loser.goal
  end

  def current_diary
    current_loser.diary
  end
end
