# frozen_string_literal: true

# :nodoc
class DiariesController < ApplicationController
  before_action :set_diary, only: %i[show update destroy]

  # GET /diaries
  def index
    @diaries = Diary.all

    render json: @diaries
  end

  # GET /diaries/1
  def show
    render json: @diary, include: { diary_entries: { limit: 10 } }
  end

  # POST /diaries
  def create
    @diary = Diary.new(diary_params)

    if @diary.save
      render json: @diary, status: :created, location: @diary
    else
      render json: @diary.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /diaries/1
  def update
    if @diary.update(diary_params)
      render json: @diary
    else
      render json: @diary.errors, status: :unprocessable_entity
    end
  end

  # DELETE /diaries/1
  def destroy
    # TODO: Does it make sense to destroy a diary without destroying
    # the user?
    @diary.destroy
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_diary
    @diary = current_loser.diary
  end

  # Only allow a trusted parameter "white list" through.
  def diary_params
    params.fetch(:diary, {})
  end
end
