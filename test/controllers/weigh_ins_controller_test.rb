require 'test_helper'

class WeighInsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @weigh_in = weigh_ins(:one)
  end

  test "should get index" do
    get weigh_ins_url, as: :json
    assert_response :success
  end

  test "should create weigh_in" do
    assert_difference('WeighIn.count') do
      post weigh_ins_url, params: { weigh_in: { weight: @weigh_in.weight } }, as: :json
    end

    assert_response 201
  end

  test "should show weigh_in" do
    get weigh_in_url(@weigh_in), as: :json
    assert_response :success
  end

  test "should update weigh_in" do
    patch weigh_in_url(@weigh_in), params: { weigh_in: { weight: @weigh_in.weight } }, as: :json
    assert_response 200
  end

  test "should destroy weigh_in" do
    assert_difference('WeighIn.count', -1) do
      delete weigh_in_url(@weigh_in), as: :json
    end

    assert_response 204
  end
end
