require 'test_helper'

class LosersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @loser = losers(:one)
  end

  test "should get index" do
    get losers_url, as: :json
    assert_response :success
  end

  test "should create loser" do
    assert_difference('Loser.count') do
      post losers_url, params: { loser: { email: @loser.email, name: @loser.name } }, as: :json
    end

    assert_response 201
  end

  test "should show loser" do
    get loser_url(@loser), as: :json
    assert_response :success
  end

  test "should update loser" do
    patch loser_url(@loser), params: { loser: { email: @loser.email, name: @loser.name } }, as: :json
    assert_response 200
  end

  test "should destroy loser" do
    assert_difference('Loser.count', -1) do
      delete loser_url(@loser), as: :json
    end

    assert_response 204
  end
end
