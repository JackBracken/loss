require 'test_helper'

class DiaryEntriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @diary_entry = diary_entries(:one)
  end

  test "should get index" do
    get diary_entries_url, as: :json
    assert_response :success
  end

  test "should create diary_entry" do
    assert_difference('DiaryEntry.count') do
      post diary_entries_url, params: { diary_entry: { calories: @diary_entry.calories, energy: @diary_entry.energy, hunger: @diary_entry.hunger, notes: @diary_entry.notes, sleep: @diary_entry.sleep, steps: @diary_entry.steps, stress: @diary_entry.stress } }, as: :json
    end

    assert_response 201
  end

  test "should show diary_entry" do
    get diary_entry_url(@diary_entry), as: :json
    assert_response :success
  end

  test "should update diary_entry" do
    patch diary_entry_url(@diary_entry), params: { diary_entry: { calories: @diary_entry.calories, energy: @diary_entry.energy, hunger: @diary_entry.hunger, notes: @diary_entry.notes, sleep: @diary_entry.sleep, steps: @diary_entry.steps, stress: @diary_entry.stress } }, as: :json
    assert_response 200
  end

  test "should destroy diary_entry" do
    assert_difference('DiaryEntry.count', -1) do
      delete diary_entry_url(@diary_entry), as: :json
    end

    assert_response 204
  end
end
