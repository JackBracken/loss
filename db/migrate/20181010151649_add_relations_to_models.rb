class AddRelationsToModels < ActiveRecord::Migration[5.2]
  def change
    add_reference :goals, :loser, foreign_key: true
    add_reference :diaries, :loser, foreign_key: true

    add_reference :diary_entries, :diary, foreign_key: true
    add_reference :measurements, :diary, foreign_key: true
    add_reference :weigh_ins, :diary, foreign_key: true
  end
end
