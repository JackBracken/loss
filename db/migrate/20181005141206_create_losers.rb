class CreateLosers < ActiveRecord::Migration[5.2]
  def change
    create_table :losers do |t|
      t.string :name
      t.string :email
      t.integer :sex

      t.timestamps
    end
    add_index :losers, :email, unique: true
  end
end
