class CreateDiaryEntries < ActiveRecord::Migration[5.2]
  def change
    create_table :diary_entries do |t|
      t.boolean :calories
      t.boolean :steps
      t.integer :sleep
      t.integer :hunger
      t.integer :stress
      t.integer :energy
      t.text :notes

      t.timestamps
    end
  end
end
