class CreateWeighIns < ActiveRecord::Migration[5.2]
  def change
    create_table :weigh_ins do |t|
      t.decimal :weight

      t.timestamps
    end
  end
end
