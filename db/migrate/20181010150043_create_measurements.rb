class CreateMeasurements < ActiveRecord::Migration[5.2]
  def change
    create_table :measurements do |t|
      t.decimal :measurement
      t.decimal :type

      t.timestamps
    end
    add_index :measurements, :type
  end
end
