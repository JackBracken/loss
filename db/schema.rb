# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_10_10_151649) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "diaries", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "loser_id"
    t.index ["loser_id"], name: "index_diaries_on_loser_id"
  end

  create_table "diary_entries", force: :cascade do |t|
    t.boolean "calories"
    t.boolean "steps"
    t.integer "sleep"
    t.integer "hunger"
    t.integer "stress"
    t.integer "energy"
    t.text "notes"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "diary_id"
    t.index ["diary_id"], name: "index_diary_entries_on_diary_id"
  end

  create_table "goals", force: :cascade do |t|
    t.decimal "weight"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "loser_id"
    t.index ["loser_id"], name: "index_goals_on_loser_id"
  end

  create_table "losers", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.integer "sex"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_losers_on_email", unique: true
  end

  create_table "measurements", force: :cascade do |t|
    t.decimal "measurement"
    t.decimal "type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "diary_id"
    t.index ["diary_id"], name: "index_measurements_on_diary_id"
    t.index ["type"], name: "index_measurements_on_type"
  end

  create_table "weigh_ins", force: :cascade do |t|
    t.decimal "weight"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "diary_id"
    t.index ["diary_id"], name: "index_weigh_ins_on_diary_id"
  end

  add_foreign_key "diaries", "losers"
  add_foreign_key "diary_entries", "diaries"
  add_foreign_key "goals", "losers"
  add_foreign_key "measurements", "diaries"
  add_foreign_key "weigh_ins", "diaries"
end
