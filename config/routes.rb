# frozen_string_literal: true

Rails.application.routes.draw do
  root 'losers#index'

  resources :losers do
    resource :goal
    resource :diary, shallow: true do
      resources :weigh_ins
      resources :measurements
      resources :diary_entries
    end
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
